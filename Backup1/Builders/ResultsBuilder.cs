﻿/*
 * Copyright (c) 2013 Paul Christensen
 * See the file license.txt for copying permission.
 */

using System;
using Newtonsoft.Json.Linq;
using ZenOfBeer.BreweryDb.Pcl.Public;
using ZenOfBeer.BreweryDb.Pcl.Utils;

namespace ZenOfBeer.BreweryDb.Pcl.Builders
{
    internal class ResultsBuilder<T> : IResultsBuilder<T>
    {
        private readonly IResultBuilder _builder;
        private string _message;
        private string _status;
        private JToken _data;

        public ResultsBuilder(IResultBuilder builder)
        {
            Guard.IsNotNull(builder);

            _builder = builder;
        }

        public IResultsContainer<T> Build()
        {
            var convertedResults = new ResultsContainer<T>
                                       {
                                           Message = _message,
                                           Status = _status
                                       };
            if (typeof (JArray) == _data.GetType())
            {
                foreach (var jToken in _data)
                {
                    ProcessJData(jToken, ref convertedResults);
                }
            }
            else
            {
                ProcessJData(_data, ref convertedResults);
            }
            return convertedResults;
        }

        public void Init()
        {
            _message = null;
            _status = null;
            _data = null;
        }

        public void SetResults(JObject jObject)
        {
            _message = StringHelpers.ParseJTokenString(jObject["message"]);
            _status = StringHelpers.ParseJTokenString(jObject["status"]);
            
            _data = jObject["data"];
        }

        private void ProcessJData(JToken jToken, ref ResultsContainer<T> convertedResults)
        {
            _builder.Init();
            _builder.SetResultData(jToken);
            convertedResults.Add((T) Convert.ChangeType(_builder.Build(), typeof (T), null));
        }
    }
}