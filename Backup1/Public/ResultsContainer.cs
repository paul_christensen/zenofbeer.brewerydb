﻿/*
 * Copyright (c) 2013 Paul Christensen
 * See the file license.txt for copying permission.
 */

using System.Collections;
using System.Collections.Generic;

namespace ZenOfBeer.BreweryDb.Pcl.Public
{
    public class ResultsContainer<T> : List<T>, IResultsContainer<T>
    {
        public ResultsContainer() : base()
        {
            
        }

        public string Message { get; set; }
        public string Status { get; set; }
    }
}