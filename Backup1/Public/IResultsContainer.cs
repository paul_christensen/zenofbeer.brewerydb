﻿/*
 * Copyright (c) 2013 Paul Christensen
 * See the file license.txt for copying permission.
 */
using System.Collections.Generic;

namespace ZenOfBeer.BreweryDb.Pcl.Public
{
    public interface IResultsContainer<T> : ICollection<T>
    {
        string Message { get; }
        string Status { get; }
    }
}